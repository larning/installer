import 'dart:io';

import 'package:flutter/material.dart';
import 'package:desktop_window/desktop_window.dart';
import 'package:installer/pages/home.dart';
import 'package:installer/utils/AppsController.dart';
import 'package:installer/utils/AppConfig.dart';
import 'package:installer/widgets/AppsProvider.dart';
import 'package:installer/widgets/ConfigProvider.dart';

AppConfig config = new AppConfig();
AppsController controller = new AppsController(config: config);

void main() {
  runApp(MyApp());
  if ((Platform.isIOS || Platform.isLinux) &&
      Platform.environment["EMBEDDED"] == "true") {
    DesktopWindow.setFullScreen(true);
    print(Platform.environment);
  }
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    controller.load(setState);
    return MaterialApp(
      title: 'Larning installer',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      builder: (BuildContext context, Widget child) => AppConfigProvider(
          appConfig: config,
          child: Builder(
              builder: (context) => AppsProvider(
                    appsController: controller,
                    child: child,
                  ))),
      routes: {"/": (context) => HomePage()},
    );
  }
}
