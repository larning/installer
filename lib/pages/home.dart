import 'package:desktop_window/desktop_window.dart';
import 'package:flutter/material.dart';
import 'package:installer/widgets/AppsProvider.dart';
import 'package:installer/widgets/FlexibleHeightGrid.dart';
import 'package:installer/widgets/LoadingScreen.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Larning installer"),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            print(await DesktopWindow.toggleFullScreen());
          },
        ),
        body: AppsProvider.of(context).loaded
            ? FlexibleHeightGrid(
                maxCrossAxisExtend: 300,
                children: [
                  ...AppsProvider.of(context)
                      .apps
                      .map(
                        (app) => Container(
                          width: 300,
                          child: Card(
                            child: FlatButton(
                              onPressed: () {},
                              child: Column(
                                children: [
                                  Image.network(
                                    app.icon,
                                    errorBuilder: (context, _, __) => Center(
                                      child: Text("No image found"),
                                    ),
                                  ),
                                  ListTile(
                                    title: Text(app.name),
                                    subtitle: Text(app.description),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      )
                      .toList(),
                  Container(
                    width: 300,
                    height: 300,
                    child: Card(
                      child: Builder(builder: (context) {
                        return FlatButton(
                          onPressed: () async {
                            TextEditingController controller =
                                TextEditingController.fromValue(
                                    TextEditingValue.empty);
                            Map<String, String> result = await showDialog(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                title: Text("Please enter valid config url"),
                                content: TextField(
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder()),
                                  controller: controller,
                                ),
                                actions: [
                                  RaisedButton(
                                    color: Colors.red,
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: Text("Abort"),
                                  ),
                                  FlatButton(
                                      onPressed: () {
                                        Navigator.of(context)
                                            .pop(<String, String>{
                                          "url": controller.value.text
                                        });
                                      },
                                      child: Text("OK"))
                                ],
                              ),
                            );
                            print(result);
                          },
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Column(
                                children: [
                                  Text("Load custom project ..."),
                                  SizedBox(
                                    height: 16,
                                  ),
                                  CircleAvatar(
                                    child: Icon(Icons.arrow_forward),
                                  )
                                ],
                                mainAxisSize: MainAxisSize.min,
                              ),
                            ),
                          ),
                        );
                      }),
                    ),
                  ),
                ],
              )
            : LoadingScreen(message: AppsProvider.of(context).statusMessage));
  }
}
