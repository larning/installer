import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:installer/utils/AppConfig.dart';
import 'package:http/http.dart' as http;

class AppsController {
  bool loaded = false;
  bool _loading = false;
  String statusMessage = "Initializing AppsController ...";
  List<LarningApp> apps = [];
  final AppConfig config;

  AppsController({@required this.config}) {
    assert(config != null);
  }

  Future<void> load(Function setState) async {
    if (this._loading) return;
    this._loading = true;
    if (!this.config.loaded) {
      setState(() {
        this.statusMessage = "Loading config ...";
      });
      await this.config.load();
    }
    setState(() {
      this.statusMessage = "Loading apps ...";
    });

    for (final String app in this.config.apps) {
      final List<String> args = List<String>.from(new RegExp(
              "(?:(?:(?<=(?<!(?<!\\\\)\\\\)\")[\\S\\s]+(?<!(?<!\\\\)\\\\)(?=\"))|(?:[^\"\\s]|(?<!\\\\)\\\\\")+)")
          .allMatches(app)
          .toList()
          .map((match) => app.substring(match.start, match.end)));
      if (args.length < 1) args.add("");
      final Map<String, List<String>> parsedArgs =
          new Map<String, List<String>>();
      final String value = args.last;
      for (int i = 0; i < args.length - 1; i++) {
        final String arg = args[i];
        final List<String> argparts = arg.split("::");
        if (argparts.length == 1) {
          if (parsedArgs.containsKey(""))
            parsedArgs[""].add(argparts[0]);
          else
            parsedArgs[""] = [argparts[0]];
        } else {
          if (parsedArgs.containsKey(argparts[0]))
            parsedArgs[argparts[0]].add(argparts.skip(1).join("::"));
          else
            parsedArgs[argparts[0]] = [argparts.skip(1).join("::")];
        }
      }
      if (parsedArgs["type"] == null || parsedArgs["type"].length < 1)
        parsedArgs["type"] = ["json"];

      final String configType = parsedArgs["type"].first;
      dynamic config;
      switch (configType) {
        case "json":
          config = jsonDecode(value);
          break;
        case "url":
          setState(() {
            this.statusMessage = "Load config for module ($value)";
          });
          config = jsonDecode((await http.get(value)).body);
          break;
        default:
          config = <String, String>{};
      }
      final Map<String, String> authors = <String, String>{};

      (config["authors"] != null
              ? config["authors"]
              : config["author"] != null ? [config["author"]] : [])
          .forEach((dynamic author) {
        if (!(author is String)) return;
        final emailMatches = r"<\S+@\S+>\s*".allMatches(author);
        final Match emailMatch =
            emailMatches.length >= 1 ? emailMatches.last : null;
        final String email = emailMatch != null
            ? author.substring(emailMatch.start, emailMatch.end)
            : "";
        final nameMatches = r"(?:[\S\s](?!<))+".allMatches(author);
        final Match nameMatch =
            nameMatches.length >= 1 ? nameMatches.first : null;
        final String name = nameMatch != null
            ? author.substring(nameMatch.start, nameMatch.end)
            : "";
        if (name != "") authors[name] = email;
      });

      apps.add(LarningApp(
          name: config["name"],
          description: config["description"],
          icon: config["icon"],
          repo: config["repo"],
          versions:
              List<String>.from(Map.from(config["versions"]).keys.toList())));
      print(config);
    }

    setState(() {
      this.loaded = true;
      this.statusMessage = "finished";
    });
  }
}

class LarningApp {
  final String name;
  final String description;
  final Map<String, String> authors;
  final String icon;
  final String repo;
  final List<String> versions;

  const LarningApp(
      {this.authors = const <String, String>{},
      this.description = "",
      this.icon = "",
      this.name = "",
      this.repo = "",
      this.versions = const <String>[]});
}
