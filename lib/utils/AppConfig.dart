import 'dart:convert';
import 'package:flutter/services.dart';

class AppConfig {
  bool loaded = false;
  List<String> apps = [];

  AppConfig({this.apps});

  Future<void> load() async {
    if (this.loaded) return;
    final contents = await rootBundle.loadString(
      'assets/config.json',
    );
    final json = jsonDecode(contents);
    this.loaded = true;
    this.apps = List<String>.from(json['apps']);
  }
}
