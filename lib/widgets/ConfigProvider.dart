import 'package:flutter/widgets.dart';
import 'package:installer/utils/AppConfig.dart';

class AppConfigProvider extends InheritedWidget {
  final AppConfig appConfig;
  const AppConfigProvider(
      {Key key, @required this.appConfig, @required Widget child})
      : assert(appConfig != null),
        super(key: key, child: child);

  static AppConfig of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<AppConfigProvider>().appConfig;

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
}
