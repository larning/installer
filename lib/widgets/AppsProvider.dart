import 'package:flutter/widgets.dart';
import 'package:installer/utils/AppsController.dart';

class AppsProvider extends InheritedWidget {
  final AppsController appsController;
  const AppsProvider(
      {Key key, @required this.appsController, @required Widget child})
      : assert(appsController != null),
        super(key: key, child: child);

  static AppsController of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<AppsProvider>().appsController;

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
}
